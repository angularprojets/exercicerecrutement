import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'exercicerecrutement';

  globalTitle = 'QUADRA INFORMATIQUE';
  globalSubTitle = 'Exercice de recrutement';

}

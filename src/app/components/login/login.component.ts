import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { RestClientService } from 'src/app/services/rest-client.service';
import { UserService } from 'src/app/services/user.service';

const STATUS_OK = "OK";
const STATUS_KO = "ERR";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Input() error!: string | null;

  @Output() userIdentifier = new EventEmitter<string>();
  
  userId!: string;
  userPassword!: string;

  userLoggedIn: boolean = false;

  loginError!: string;

  constructor(private router: Router, private userService: UserService, private restClientServ: RestClientService) { }

  ngOnInit(): void {
  }

  /**
   * Send user ID
   */
  sendUserId() {
    console.log("@@ User ID sent="+this.userId);
    this.userIdentifier.emit(this.userId);
  }

  /**
   * Check password validation
   */
  checkPasswordValidator(control: FormControl): object | null {
    const str: string = control.value;
    if (str[0] >= 'A' && str[0] <= 'Z') {
        return null;
    } else {
        return { erreur: 'Prénom non valide' };
    }
  } 

  /**
   * Demande de login
   */
  submit() {
      console.log("@@ Login submitted: ID="+this.userId+", Pwd="+this.userPassword);

      localStorage.setItem('isConnected', 'true');
      localStorage.setItem('userId', this.userId);
      this.userService.userIsLoggedIn();
 
      this.userLogin();

    }

  userLogin(): void {
    this.restClientServ.userAuth(this.userId, this.userPassword).subscribe(
      (data: any) => {
        console.log("@@ UserLogin: returned data=" + JSON.stringify(data));
         this.sendUserId();
        this. userLoggedIn = true;

        this.restClientServ.setAuthData(data);

        //--- Pour tester
        this.router.navigateByUrl('/dashboard');

      },
      (err: any) => {
        console.log("&& userLogin Erreur: " + JSON.stringify(err));
        this.loginError = err.error;
      },
      () => console.log("&& userLogin terminé !")
    );
  }

}




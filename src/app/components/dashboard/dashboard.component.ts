import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RestClientService } from 'src/app/services/rest-client.service';

const REQ_TODO_CREATION: string = "creation";
const REQ_TODO_LIST: string = "list";
const COLOR_RED: string = "red";
const COLOR_WHITE: string = "white";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  listColor: string = COLOR_RED;
  newTaskColor: string = COLOR_WHITE;

  userConnected: boolean = false;

  addOrUpdate: number = 0;

  connectedUserId!: string;

  tasks!: any[];
  columns: any[] = ['todo_id','todo_user_id', 'todo_label','todo_date', 'todo_is_done'];

  taskColumns: any[] = ['ID', 'USER ID', 'LABEL', 'DATE', 'IS DONE', 'ACTION', 'ACTION'];
  
  currentTodo: any = {todo_id:'',  todo_user_id:'', todo_label:'', todo_date:'', todo_is_done:0};

  constructor(private restClientServ: RestClientService, private route: ActivatedRoute) { 

    this.listColor = COLOR_RED;
    this.newTaskColor = COLOR_WHITE;

    this.route.parent?.queryParams.subscribe(params => {
      console.log("@@@@ queryParams: "+JSON.stringify(params));
      if (params.request === REQ_TODO_CREATION) {
        this.addOrUpdate = 1;
        this.listColor = COLOR_WHITE;
        this.newTaskColor = COLOR_RED;
      }
      else
        if (params.request === REQ_TODO_LIST) {
          this.addOrUpdate = 0;
          this.listColor = COLOR_RED;
          this.newTaskColor = COLOR_WHITE;
        }
    });
 }

  /**
   * Dashbord initialization 
   */
  ngOnInit(): void {
    this.connectedUserId = localStorage.getItem('userId') as string;
    console.log("@@ connectedUserId="+this.connectedUserId);

    /* Load tasks list */
    this.loadTasksList();

    console.log("@@ Colors: LIST="+this.listColor+", NEWTODO="+this.newTaskColor);
  }

  getConnectedUserId(userId: string) {
    console.log("@@ Connected user ID="+userId);
    this.connectedUserId = userId;
  }

  /**
   * 
   * Load tasks list
   */
  loadTasksList() {
    this.restClientServ.getTasksList().subscribe(
      (data: any) => {
        this.tasks = data as any[]; 
        console.log("$$ TASKS List: "+JSON.stringify(data))
      },
      (err: any) => {console.log("&& userLogin Erreur: " + JSON.stringify(err))},
      () => console.log("&& Task list terminé !")
    );
  }

  /**
   * Modifier la tâche
   */
  updateTask(task: any) {
    console.log("@@ Tâche à mettre à jour="+JSON.stringify(task));
    this.addOrUpdate = 2;

    this.currentTodo = task;
  }

  /**
   * Supprimer la tâche
   */
  deleteTask(task: any) {
    console.log("@@ Tâche à supprimer="+JSON.stringify(task));

    /* Update task */
    this.restClientServ.deleteTask(task).subscribe(
      (data: any) => {
        console.log("$$ TASK DELETE resp: "+JSON.stringify(data));
      },
      (err: any) => {console.log("&& Delete task Erreur: " + JSON.stringify(err))},
      () => {
        console.log("&& Update task terminé !");
        this.loadTasksList();
        this.listColor = COLOR_RED;
        this.newTaskColor = COLOR_WHITE;
      }
    );

  }

  addTaskSubmit() {
    console.log("@@ Creation submitted: current task="+JSON.stringify(this.currentTodo));
    this.addOrUpdate = 0;

    /* Add task */
    this.restClientServ.addTask(this.currentTodo).subscribe(
      (data: any) => {
        console.log("$$ TASK ADD resp: "+JSON.stringify(data));
      },
      (err: any) => {console.log("&& Add task Erreur: " + JSON.stringify(err))},
      () => {
        console.log("&& add task terminé !");
        this.loadTasksList();
        this.listColor = COLOR_RED;
        this.newTaskColor = COLOR_WHITE;
      }
    );

  }

  updateTaskSubmit() {
    console.log("@@ Update submitted: current task="+JSON.stringify(this.currentTodo));
    this.addOrUpdate = 0;

    /* Update task */
    this.restClientServ.updateTask(this.currentTodo).subscribe(
      (data: any) => {
        console.log("$$ TASK UPDATE resp: "+JSON.stringify(data));
      },
      (err: any) => {console.log("&& Update task Erreur: " + JSON.stringify(err))},
      () => {
        console.log("&& Update task terminé !");
        this.loadTasksList();
        this.listColor = COLOR_RED;
        this.newTaskColor = COLOR_WHITE;
      }
    );
  }

  getListReq() {
    return {request: 'list', reqId:Date.now()};
  }

  getCreationReq() {
    return {request: 'creation', reqId:Date.now()};
  }

}

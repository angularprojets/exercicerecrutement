import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { OnlyLoggedInUsersGuard } from './guards/only-logged-in-user.guard';

const routes: Routes = [
  { path: '', redirectTo: 'auth', pathMatch: 'full' },
  { path: 'auth', component: LoginComponent },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [OnlyLoggedInUsersGuard],
  },
  {
    path: 'dashboardList',
    component: DashboardComponent,
    canActivate: [OnlyLoggedInUsersGuard],
  },
  {
    path: 'dashboardCreation',
    component: DashboardComponent,
    canActivate: [OnlyLoggedInUsersGuard],
  },

  { path: '**', component: LoginComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  userLoggedIn: boolean = false;

  constructor() { }

  userIsLoggedIn() {
    console.log("@@ User logged in")
    this.userLoggedIn = true;
  }

  userIsLoggedOut() {
    console.log("@@ User logged out")
    this.userLoggedIn = false;
  }

  isUserLoggedIn(): boolean {
    console.log("@@ User status="+this.userLoggedIn);
    console.log("## User status="+localStorage.getItem('isConnected'));

    return this.userLoggedIn;
  }

}

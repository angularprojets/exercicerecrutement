import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const headers = { 'Content-Type': 'application/json' };

const LOGIN_URL = "https://test1.quadra-informatique.fr/api/auth/login";
const TASKSLIST_URL = "https://test1.quadra-informatique.fr/api/todo/list";
const REST_URL = "https://test1.quadra-informatique.fr/api/todo";

@Injectable({
  providedIn: 'root'
})
export class RestClientService {

  loginData!: any;

  constructor(private httpClient: HttpClient) { }

  /**
   * Request headers
   */
  getHeaders(): any {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${this.loginData.token}`,
      'user-id': `${this.loginData.user.id}`
    })

  }
  /**
   * Authentication
   */
  public userAuth(userId: string, userPwd: string): any {
    let  body = { "login": userId, "password": userPwd};
    return this.httpClient.post<any>(LOGIN_URL, body, { headers });
  }  

  /**
   * Set the user logged data
   */
  setAuthData(loginData: any) {
    this.loginData = loginData;

    console.log("&& Authentication: "+JSON.stringify(this.loginData));
  }

  /**
   * Get tasks list
   */
  public getTasksList(): any {
     return this.httpClient.get<any[]>(TASKSLIST_URL, { headers: this.getHeaders() });
  }  

 /**
   * Add task
   */
  public addTask(task: any): any {
    let body = {"todo_label": task.todo_label, "todo_is_done": task.todo_is_done};
    console.log("&& addTask: "+JSON.stringify(body));

     return this.httpClient.post<any[]>(REST_URL, body, { headers: this.getHeaders() });
  }  

  /**
   * Update task
   */
  public updateTask(task: any): any {
    let body = {"todo_id": task.todo_id, "todo_label": task.todo_label, "todo_is_done": task.todo_is_done}
    console.log("&& updateTask: "+JSON.stringify(body));

     return this.httpClient.post<any[]>(REST_URL, body, { headers: this.getHeaders() });
  } 

  /**
   * Delete task
   */
  public deleteTask(task: any): any {
     return this.httpClient.delete(REST_URL+'/'+`${task.todo_id}`, { headers: this.getHeaders() });
  } 

}

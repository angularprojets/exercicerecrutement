DETAIL SUR L'APPLICATION EXERCICERECRUTEMENT
============================================

L'application comporte 2 pages:

1) Une page d'authentification (login)
2) Une page d'accès aux données (dashboard).

Ces deux pages sont construites sur à l'aide de deux composant (components):
    - LoginComponent: chargé de l'authentification
    - DashboardComponent: chargé de géré de la page d'accueil (ou tableau de board):
        . Liste des tâches;
        . ajout de tâche;
        - modification ou suppression de la tâche sélectionnée;
        . déconnection
        
Le look and feel est fait en Material.
L'application est responsive.

L'accès à la page dashboard est contrôlé par un guard: 'only-logged-in-user'

Les Pages
---------
1) La page d'authentification
    Elle comporte un formulaire (en Material) avec 2 champs:
    - L'identifiant de l'utilisateur
    - Et le mot de passe.

2) La page principale 'dashboard' comporte un menu à 2 items:
    - List des todo: 'Les tâches' qui affiche la liste des tâches affectées à l'utilisateur actuel
    - Ajout d'une tâche: 'Nouvelle tâche' qui permet de créer une nouvelle tâche affectée à l'utilisateur actuel.
        On utilise pour cela un formulaire.

    On y accède qu'après une authentification par la page 'Login'

    Elle affiche le nom de l'utilsateur connecté.

    Elle comporte en outre un bouton 'Déconnextion' pour revenir à la page de Login pour changer éventuellement d'utilisateur.

Remarque:
---------
Volontairement je n'ai pas mis de bouton 'Fermer' qui ne me parait pas nécessaire dans la mesure où n'importe quel autre choix sur la page enlève le formulaire.
